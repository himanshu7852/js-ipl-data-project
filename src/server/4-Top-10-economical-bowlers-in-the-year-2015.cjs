const express=require('express')
const path=require('path')
const port = require('../../config')
const app=express();


const csvtojson=require('csvtojson');

// const csvFilePathMatches='../data/matches.csv'
// const csvFilePathDeleveries='../data/deliveries.csv'
const csvFilePathMatches=path.join(__dirname,'../data/matches.csv')
const csvFilePathDeleveries=path.join(__dirname,'../data/deliveries.csv')

const fs=require('fs')

const routerFour =(req,res)=>{

    csvtojson()
    
    .fromFile(csvFilePathMatches)
    .then((matches)=>{
    
        const matchIDof2015 = matches.filter((year)=>{
            return year.season.includes('2015')
        }).map((matchId)=>{
            return matchId.id;
        })
        
        csvtojson()
        .fromFile(csvFilePathDeleveries)
        .then((deliveries)=>{
            const economicalBowlerIn2015 =deliveries.reduce(( acc, currentData)=>{
                if(matchIDof2015.includes(currentData.match_id)){
    
                    if(acc[currentData.bowler])
                    {
                        if(acc[currentData.bowler].bye_runs || acc[currentData.bowler].legbye_runs)
                        {
                            acc[currentData.bowler].runs+=Number(0);
                        }
                        else{
                            acc[currentData.bowler].runs+=Number(currentData.total_runs);
                        }
                        //wide and no balls
                        if((acc[currentData.bowler].wide_runs || acc[currentData.bowler].noball_runs))
                        {
                          acc[currentData.bowler].balls+=Number(0);
                        }else
                        {
                          acc[currentData.bowler].balls+=Number(1)
                        }
    
                    }
                    else{
                        acc[currentData.bowler] = {};
        
                        acc[currentData.bowler].runs = Number(currentData.total_runs);
    
                        if((acc[currentData.bowler].wide_runs || acc[currentData.bowler].noball_runs))
                        {
                            acc[currentData.bowler].balls+= Number(0);
                        }else
                        {
                            acc[currentData.bowler].balls = Number(1)
                        }
                    }
                }          
                return acc;
            },{})
    
            const allBowlerEconomy = Object.entries(economicalBowlerIn2015).map((data)=>{
                const name = data[0];
                let run = data[1].runs;
                let ball = data[1].balls;
                let economy = (run/ball)*6;
                return [name,economy];
            })
            allBowlerEconomy.sort((a, b)=>{
                return a[1]-b[1];
            })
            const top10EconomicalBowler = allBowlerEconomy.slice(0,10);
            res.json(top10EconomicalBowler)
           // console.log(top10EconomicalBowler)
            // fs.writeFileSync('../public/output/4-Top-10-economical-bowlers-in-the-year-2015.json',JSON.stringify(top10EconomicalBowler,null,2),"utf-8",(err)=>{
            //     if(err)console.log(err)
            //  })    
        })
        .catch((err)=>{
            next({
                Message:err,
                status:400
            })
        })        
    })
    .catch((err)=>{
        next({
            Message:err,
            status:400
        })
    })
}
module.exports=routerFour