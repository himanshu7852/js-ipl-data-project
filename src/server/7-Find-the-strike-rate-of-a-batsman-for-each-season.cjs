const express=require('express')
const path=require('path')
const port = require('../../config')
const app=express();

const csvtojson=require('csvtojson');

const csvFilePathMatches=path.join(__dirname,'../data/matches.csv')
const csvFilePathDeleveries=path.join(__dirname,'../data/deliveries.csv')

const fs=require('fs')
const routerSeven =(req,res)=>{

  csvtojson()
  
  .fromFile(csvFilePathMatches)
  .then((matches)=>{
  
      const seasonId = matches.reduce((matchId, match) => {
          matchId[match.id] = match.season;
    
          return matchId;
        }, {});
    
  
      
      //console.log(matchIds);
      csvtojson()
      .fromFile(csvFilePathDeleveries)
      .then((deliveries)=>{
  
  
          const playerData = deliveries.reduce((batsmanData, delivery) => {
              let season = seasonId[delivery.match_id];
    
              if (batsmanData[delivery.batsman]) {
                if (batsmanData[delivery.batsman][season]) {
                  batsmanData[delivery.batsman][season].totalRuns += Number(
                    delivery.total_runs
                  );
                  batsmanData[delivery.batsman][season].totalBallsFaced += 1;
                  batsmanData[delivery.batsman][season].strikeRate = Number(
                    (batsmanData[delivery.batsman][season].totalRuns /
                      batsmanData[delivery.batsman][season].totalBallsFaced) *
                      100
                  ).toFixed(2);
                } else {
                  batsmanData[delivery.batsman][season] = {};
                  batsmanData[delivery.batsman][season].totalRuns = Number(
                    delivery.total_runs
                  );
                  batsmanData[delivery.batsman][season].totalBallsFaced = 1;
                }
              } else {
    
                batsmanData[delivery.batsman] = {};
                batsmanData[delivery.batsman][season] = {};
                batsmanData[delivery.batsman][season].totalRuns = Number(
                  delivery.total_runs
                );
                batsmanData[delivery.batsman][season].totalBallsFaced = 1;
              }
    
              return batsmanData;
            }, {});
    
            const strikeRateOfAllBatsman = Object.entries(playerData).reduce(
              (batsman, batsmanStrikeDetail) => {
                batsman[batsmanStrikeDetail[0]] = Object.entries(
                  batsmanStrikeDetail[1]
                ).reduce((year, strikeValue) => {
                  year[strikeValue[0]] = strikeValue[1].strikeRate;
    
                  return year;
                }, {});
    
                return batsman;
              }, {});

              res.json(strikeRateOfAllBatsman) 
        //console.log(top10Economy);
        // fs.writeFileSync('../public/output/7-Find-the-strike-rate-of-a-batsman-for-each-season.json',JSON.stringify(strikeRateOfAllBatsman,null,2),"utf-8",(err)=>{
        //       if(err)console.log(err)
        //    })    
  })
  .catch((err)=>{
    next({
        Message:err,
        status:400
    })
})
  })
  .catch((err)=>{
    next({
        Message:err,
        status:400
    })
})
}
module.exports=routerSeven