//Find a player who has won the highest number of Player of the Match awards for each season
const express=require('express')
const path=require('path')
const port = require('../../config')
const app=express();
const csvtojson=require('csvtojson');

const csvFilePath=path.join(__dirname,'../data/matches.csv')

const fs=require('fs')
const routerSix =(req,res)=>{

    csvtojson()
    .fromFile(csvFilePath)
    .then((matches)=>{
        //console.log(matches[1])
        const getAllPlayerOfTheMatch=matches.reduce((player,match)=>{
            if(player[match.season]===undefined)
            {
                player[match.season]={}
            } 
            else{
                if(player[match.season][match.player_of_match])
                {
                    player[match.season][match.player_of_match]+=1;
                }
                else{
                player[match.season][match.player_of_match]=1;
                }
            }
            return player
        },{})
        const getHighestPlayerOfTheMatch=Object.entries(getAllPlayerOfTheMatch).reduce((acc,currPlayer)=>{
            const playerData=Object.entries(currPlayer[1]).sort((player1,player2)=>{
                return player2[1]-player1[1];
            }).slice(0,1);
    
            acc[currPlayer[0]]=Object.fromEntries(playerData);
            return acc;
        },{})
        
        //console.log(getHighestPlayerOfTheMatch)
        res.json(getHighestPlayerOfTheMatch)
    
        // fs.writeFileSync('../public/output/6-Find-a-player-who-has-won-the-highest-number-of-Player-of-the-Match-awards-for-each-season.json',JSON.stringify(getHighestPlayerOfTheMatch,null,2),"utf-8",(err)=>{
        //     if(err)console.log(err)
        //  })
    })
    .catch((err)=>{
        next({
            Message:err,
            status:400
        })
    })
}
module.exports=routerSix