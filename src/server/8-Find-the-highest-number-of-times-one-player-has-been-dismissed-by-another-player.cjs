//Find the strike rate of a player_dismissed for each season
const express=require('express')
const path=require('path')
const app=express();

const csvtojson=require('csvtojson');

const csvFilePathDeleveries=path.join(__dirname,'../data/deliveries.csv')

const fs=require('fs')
const routerEight =(req,res)=>{

    csvtojson()
    .fromFile(csvFilePathDeleveries)
    .then((deliveries)=>{
    
        const allWickets = deliveries.reduce((acc , currBowl)=>{
            if(currBowl.dismissal_kind == 'run out' || currBowl.player_dismissed == '' || currBowl.dismissal_kind == 'hit wicket'||currBowl.dismissal_kind=='retired hurt')
            {
                             
            }
            else{
                if(acc[currBowl.bowler])
                {
                    if(acc[currBowl.bowler][currBowl.player_dismissed])
                    {
                        acc[currBowl.bowler][currBowl.player_dismissed]+=1;
                    }else
                    {
                        acc[currBowl.bowler][currBowl.player_dismissed]=1;                      
                    }
                }else
                {
                    acc[currBowl.bowler]={};
                      
                   acc[currBowl.bowler][currBowl.player_dismissed]=1;
                }                
              } 
          return acc;
        },{})
    
        let getHighestWicketsAgainstOne = Object.entries(allWickets).map((bowler)=>{
    
            let player_dismissed=Object.entries(bowler[1]).sort((player1, player2)=>{
               return player2[1] - player1[1];
            })
            return [bowler[0],player_dismissed[0]];
        }).sort((player3, player4) =>{
    
            return player4[1][1] - player3[1][1];
        });
    
        getHighestWicketsAgainstOne =getHighestWicketsAgainstOne[0];

        res.json(getHighestWicketsAgainstOne)
        //console.log(getHighestWicketsAgainstOne);
        // fs.writeFileSync('../public/output/8-Find-the-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json',JSON.stringify(getHighestWicketsAgainstOne,null,2),"utf-8",(err)=>{
        //         if(err)console.log(err)
        // })
    })
    .catch((err)=>{
        next({
            Message:err,
            status:400
        })
    })
}
module.exports=routerEight