const express=require('express')
const path=require('path')
const port = require('../../config')
const app=express();
const csvtojson=require('csvtojson');

const csvFilePath='/home/himanshu/MountBlue/js-ipl-data-project/src/data/matches.csv'

const fs=require('fs')
const routerTwo =(req,res)=>{

    csvtojson()
    .fromFile(csvFilePath)
    .then((matches)=>{
        const allMatchesWins=matches.reduce((acc,curr)=>{
            if(acc[curr.season]===undefined)
            {
                acc[curr.season]={};
            }
                   
                if(acc[curr.season][curr.winner])
                {
                    acc[curr.season][curr.winner]+=1;
                }
                else{
                    acc[curr.season][curr.winner]=1;
                }
            
            return acc;
        },{})
        res.json(allMatchesWins)
        //console.log(allMatchesWins)  
    
        // fs.writeFileSync('/home/himanshu/MountBlue/js-ipl-data-project/src/public/output/2-matches-won-per-team-per-year.json',JSON.stringify(allMatchesWins,null,2),"utf-8",(err)=>{
        //     if(err)console.log(err)
        //  })
    })
    .catch((err)=>{
        next({
            Message:err,
            status:400
        })
    })
}
module.exports=routerTwo