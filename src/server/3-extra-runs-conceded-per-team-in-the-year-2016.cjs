const express=require('express')
const path=require('path')
const port = require('../../config')
const app=express();

const csvtojson=require('csvtojson');

const csvFilePathMatches=path.join(__dirname,'../data/matches.csv')
const csvFilePathDeleveries=path.join(__dirname,'../data/deliveries.csv')

const fs=require('fs')
const routerThree =(req,res)=>{

    csvtojson()
    .fromFile(csvFilePathMatches)
    .then((matches)=>{
    
        const matchID=matches.filter((match)=>{
            return match.season.includes('2016')
        }).map((findID)=>{
            return findID.id;
        })
        //console.log(matchID);
    
        csvtojson()
        .fromFile(csvFilePathDeleveries)
        .then((deliveries)=>{
    
            const getExtraRunsOfAllTeams=deliveries.reduce((extras,currMatch)=>{
                if(matchID.includes(currMatch.match_id))
                {
                    if(extras[currMatch.bowling_team])
                    {
                        extras[currMatch.bowling_team]+=Number(currMatch.extra_runs);
                    }
                    else{
                        extras[currMatch.bowling_team]=Number(currMatch.extra_runs);
                    }
                }
                return extras;
            },{})
            res.json(getExtraRunsOfAllTeams)
            //console.log(getExtraRunsOfAllTeams)
            // fs.writeFileSync('../public/output/3-extra-runs-conceded-per-team-in-the-year-2016.json',JSON.stringify(getExtraRunsOfAllTeams,null,2),"utf-8",(err)=>{
            //     if(err)console.log(err)
            //  })
        })
        .catch((err)=>{
            next({
                Message:err,
                status:400
            })
        })
    })
    .catch((err)=>{
        next({
            Message:err,
            status:400
        })
    })
}
module.exports=routerThree