const express=require('express')
const path=require('path')
const app=express();

const csvtojson=require('csvtojson');

const csvFilePathDeleveries=path.join(__dirname,'../data/deliveries.csv')

const fs=require('fs')
const routerNine =(req,res)=>{

    csvtojson()
    .fromFile(csvFilePathDeleveries)
    .then((deliveries)=>{
        
        const totalRunsByBowler = deliveries.filter((ball)=>{
    
            return ball.is_super_over !== '0';
           }).reduce((bowlerList, currBall)=>{
            
            if(bowlerList[currBall.bowler])
            {
                bowlerList[currBall.bowler].runs+=Number(currBall.total_runs);
                bowlerList[currBall.bowler].ball +=1;
                bowlerList[currBall.bowler].economyValue = ((bowlerList[currBall.bowler].runs/bowlerList[currBall.bowler].ball)*6);
            }else
            {
                bowlerList[currBall.bowler]= {};
                bowlerList[currBall.bowler].runs = Number(currBall.total_runs);
                bowlerList[currBall.bowler].ball =1;
            }
        
            return bowlerList;
        },{});
    
        const bowlerEconomy = Object.entries(totalRunsByBowler).reduce((bowlerName, currBower)=>
        {
    
            bowlerName[currBower[0]] = currBower[1].economyValue;
            return bowlerName;
        },{});
    
        const bestEconomicalBowler = Object.fromEntries(Object.entries(bowlerEconomy).sort((player1, player2)=>{
            return player1[1]- player2[1];
        }).slice(0, 1));

        res.json(bestEconomicalBowler)    
        // console.log(bestEconomicalBowler)
    
        // fs.writeFileSync('../public/output/9-Find-the-bowler-with-the-best-economy-in-super-overs.json',JSON.stringify({bestEconomicalBowler},null,2),"utf-8",(err)=>{
        //         if(err)console.log(err)
        //      })
    })
    .catch((err)=>{
        next({
            Message:err,
            status:400
        })
    })
}
module.exports=routerNine