// Find the number of times each team won the toss and also won the match
const express=require('express')
const path=require('path')
const port = require('../../config')
const app=express();
const csvtojson=require('csvtojson');

const csvFilePath=path.join(__dirname,'../data/matches.csv')

const fs=require('fs')

const routerFive =(req,res)=>{

    csvtojson()
    .fromFile(csvFilePath)
    .then((matches)=>{
        //console.log(matches[1])
    
        const getTeamWonTossAndGame=matches.reduce((team,currMatch)=>{
            if(currMatch.toss_winner==currMatch.winner)
            {
                if(team[currMatch.winner])
                {
                    team[currMatch.winner]+=1;
                }
                else{
                    team[currMatch.winner]=1;
                }
            }
            return team;
        },{})
        res.json(getTeamWonTossAndGame)    
        //console.log(getTeamWonTossAndGame);
            
        // fs.writeFileSync('../public/output/5-Find-the-number-of-times-each-team-won-the-toss-and-also-won-the-match.json',JSON.stringify(getTeamWonTossAndGame,null,2),"utf-8",(err)=>{
        //    if(err)console.log(err)
        // })
    })
    .catch((err)=>{
        next({
            Message:err,
            status:400
        })
    })
}
module.exports=routerFive