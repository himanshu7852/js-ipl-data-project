const express=require('express')
const path=require('path')
const port = require('./config.js')
const http=require('http')
const app=express();
const router=express.Router()
const requestID=require('express-request-id')
const fs=require('fs')

app.get('/',(req,res)=>{
    res.send(`<h2>IPL Project<h2>
    <h3>/matchesPerYear<h3>
    <h3>/allMatchWins<h3>
    <h3>/extraRuns<h3>
    <h3>/economicalBowler<h3>
    <h3>/getTeamThatWinTossAndMatch<h3>
    <h3>/highestPOTM<h3>
    <h3>/batsmanStrikeRate<h3>
    <h3>/highestWicketsAgainsOne<h3>
    <h3>/bestEconomyInSuperOver<h3>

    `)
})
app.use(requestID())
app.use(function(req,res,next){
    fs.appendFile('logFiles.log',` url-${req.url} :: RequestID-${req.id}`+'\n',(err)=>{
        if(err){
            console.log(err)
        }
    })
    next()
})

router.get('/matchesPerYear',require('./src/server/1-matches-per-year.cjs'));

router.get('/allMatchWins',require('./src/server/2-matches-won-per-team-per-year.cjs'));

router.get('/extraRuns',require('./src/server/3-extra-runs-conceded-per-team-in-the-year-2016.cjs'));

router.get('/economicalBowler',require('./src/server/4-Top-10-economical-bowlers-in-the-year-2015.cjs'));

router.get('/getTeamThatWinTossAndMatch',require('./src/server/5-Find-the-number-of-times-each-team-won-the-toss-and-also-won-the-match.cjs'));

router.get('/highestPOTM',require('./src/server/6-Find-a-player-who-has-won-the-highest-number-of-Player-of-the-Match-awards-for-each-season.cjs'));

router.get('/batsmanStrikeRate',require('./src/server/7-Find-the-strike-rate-of-a-batsman-for-each-season.cjs'));

router.get('/highestWicketsAgainsOne',require('./src/server/8-Find-the-highest-number-of-times-one-player-has-been-dismissed-by-another-player.cjs'));

router.get('/bestEconomyInSuperOver',require('./src/server/9-Find-the-bowler-with-the-best-economy-in-super-overs.cjs'));

router.get('/log',(req,res,next)=>{
    
    fs.readFile('logFiles.log','utf-8',(err,data)=>{
        if(err)
        {
            next({
                Message:'Server Error',
                status:400
            })
        }
        else{
            res.send(data)
        }
    })
})
app.use(router)

app.use((err,req,res,next)=>{
    res.status(err.status).json({Message: err.message})
})

app.use((req,res,next)=>{
    res.status(404).json({Message: 'Not a valid URL'})
})

app.listen(port, () => {
    console.log(`${port} Server running`)
})